/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DBFiles;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daniel
 */
@Entity
@Table(name = "IMAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Image.findAll", query = "SELECT i FROM Image i"),
    @NamedQuery(name = "Image.findByImgid", query = "SELECT i FROM Image i WHERE i.imgid = :imgid"),
    @NamedQuery(name = "Image.findByTitle", query = "SELECT i FROM Image i WHERE i.title = :title"),
    @NamedQuery(name = "Image.findBySubdate", query = "SELECT i FROM Image i WHERE i.subdate = :subdate"),
    @NamedQuery(name = "Image.findByVotes", query = "SELECT i FROM Image i WHERE i.votes = :votes")})
public class Image implements Serializable {
    @OneToMany(mappedBy = "cimgID")
    private Collection<Comment> commentCollection;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VOTES")
    private int votes;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "IMGID")
    private Integer imgid;
    @Size(max = 20)
    @Column(name = "TITLE")
    private String title;
    @Column(name = "SUBDATE")
    @Temporal(TemporalType.DATE)
    private Date subdate;
    @ManyToMany(mappedBy = "imageCollection")
    private Collection<Tag> tagCollection;
    @JoinColumn(name = "U_IMGID", referencedColumnName = "UID")
    @ManyToOne
    private User uImgid;

    public Image() {
    }

    public Image(Integer imgid) {
        this.imgid = imgid;
    }
    
    public Image(String newTitle, User owner,java.sql.Date deitti){
        this.title = newTitle;
        this.uImgid = owner;
        this.subdate = deitti;
    }
    
    public Image(String newTitle,java.sql.Date deitti){
        this.title = newTitle;  
        this.subdate = deitti;      
    }

    public Integer getImgid() {
        return imgid;
    }

    public void setImgid(Integer imgid) {
        this.imgid = imgid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getSubdate() {
        return subdate;
    }

    public void setSubdate(Date subdate) {
        this.subdate = subdate;
    }


    @XmlTransient
    public Collection<Tag> getTagCollection() {
        return tagCollection;
    }

    public void setTagCollection(Collection<Tag> tagCollection) {
        this.tagCollection = tagCollection;
    }

    public User getUImgid() {
        return uImgid;
    }

    public void setUImgid(User uImgid) {
        this.uImgid = uImgid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imgid != null ? imgid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Image)) {
            return false;
        }
        Image other = (Image) object;
        if ((this.imgid == null && other.imgid != null) || (this.imgid != null && !this.imgid.equals(other.imgid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Title: " + this.title + " -- ImgID: " + this.imgid;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    @XmlTransient
    public Collection<Comment> getCommentCollection() {
        return commentCollection;
    }

    public void setCommentCollection(Collection<Comment> commentCollection) {
        this.commentCollection = commentCollection;
    }
    
}
